<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_sports' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'V3}JtNC+y(^iX9]2g.Kd4Ci($0&-U2YziAkX;JRXiLd]H_#TgmmmyB-2aS*N|7wH' );
define( 'SECURE_AUTH_KEY',  'C^zKyX2h%@^+/;@St*P>$!:sM5nLT:<|}lyF.vXg+1|@>]x^|6tcCGWe9$,5O8xK' );
define( 'LOGGED_IN_KEY',    '?<[fQ<4Uzu?|>]:Fk_[P-ay$$^BHhnosL>S+A--5n|yuc!lmI.v&A9H#I`SY}3Jf' );
define( 'NONCE_KEY',        '{$[<t^$$O}@]c0j7LG&&Qe5rn^.)r9&191I(p9bS2Rv>Vx[E~SHO`[?:xG_M{{PZ' );
define( 'AUTH_SALT',        'Ap|rstM*-!}8%UBCm+4P)B)0ga<k}B.<fb!zwb~,@IHe>@Q!||s!YIL|~r$qg/w&' );
define( 'SECURE_AUTH_SALT', 'oY$aytfm/Ae+k_J!^q_K4Y2^kh]JZXEw?b,tf Khwrh??y=7|[IJ~pPUU4Fd~W,?' );
define( 'LOGGED_IN_SALT',   'O<y&mQQ$o)b:@%n@ZRzW!65vm>iDjK;0D*.WJq(wV^XTu?JEE$0+@= AmVn7H@U`' );
define( 'NONCE_SALT',       't531xVzGY*$vRy$m,1< sf%DJ0~$}!l%%$MNLC+X_UQ:.NU7BK .uN&R(1{$%9eC' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
